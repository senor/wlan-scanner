import network
import json
import st3m.run
from st3m.application import Application, ApplicationContext
from ctx import Context
from st3m.input import InputState
import leds
import math


def get_value(led,wlan,phase):
    return math.sin(0.05*math.pi*led*wlan+phase)/2+0.5

class WlanScanner(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.path = "/sd/scanner/scan.json"
        self.wlan = network.WLAN(network.STA_IF)
        self.load_data()
        self.last_scan = []
        self._scale = 1.0
        self._led = 0.0
        self._phase = 0.0

    def load_data(self):
        try:
            with open(self.path) as f:
                jsondata = f.read()
            self.data = json.loads(jsondata)
        except OSError:
            self.data = {}
    
    def save_data(self):
        jsondata = json.dumps(self.data)
        with open(self.path, "w") as f:
            f.write(jsondata)
            f.close()   

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = "Camp Font 2"
        ctx.font_size = 75
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(0, 1, 0)
        ctx.move_to(0,0)
        ctx.text(str(len(self.last_scan)))
        ctx.move_to(0,40)
        ctx.font_size = 30
        ctx.text(str(len(self.data)))

        leds.set_all_rgb(0,1,0)
        leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        self.last_scan = self.wlan.scan()
        for res in self.last_scan:
            self.data[res[0]] = res
        
        self._phase += delta_ms / 1000
        self._scale = math.sin(self._phase)
        self._led += delta_ms * (1 - 1 / len(self.last_scan)) / 45
        self._led = self._led % 40

    def on_exit(self):
        self.save_data()

if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(WlanScanner(ApplicationContext()))